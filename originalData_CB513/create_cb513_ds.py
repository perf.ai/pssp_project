"""
Uses the DATASETS files to create new datasets for CB513 based
on the specified number of amino acids (ADD_AMINO_ACIDS).
"""

import os, sys

ADD_AMINO_ACIDS = 7 # 7 + 1 + 7 = 15 amino acids per row
DATASETS = ['trainSet0.txt', 'testSet0.txt',
			'trainSet1.txt', 'testSet1.txt',
			'trainSet2.txt', 'testSet2.txt',
			'trainSet3.txt', 'testSet3.txt',
			'trainSet4.txt', 'testSet4.txt',
			'trainSet5.txt', 'testSet5.txt',
			'trainSet6.txt', 'testSet6.txt',
			'trainSet7.txt', 'testSet7.txt',
			'trainSet8.txt', 'testSet8.txt',
			'trainSet9.txt', 'testSet9.txt']

FOLDER_NAME = 'plus{0}_CB513'.format(ADD_AMINO_ACIDS)

if not os.path.exists(FOLDER_NAME):
	os.makedirs(FOLDER_NAME)

protein_name = None
hssp_file = None
CATEGORIES = ['C', 'E', 'H']

def enumerate_cat(labels):
	for i, cat in enumerate(CATEGORIES):
		labels = labels.replace(cat, str(i))
	return labels

def get_zero_lines(num_of_lines):
	if (num_of_lines < 1):
		return ""
	zeros = (("0," * 20) + '\n') * num_of_lines
	return zeros

for dataset_name in DATASETS:
	# dataset_name = DATASETS[0]
	print('Preparing {0}... Missing hssp files:'.format(dataset_name))
	output_file = './{0}/plus{1}_{2}'.format(FOLDER_NAME, ADD_AMINO_ACIDS, dataset_name)

	with open(dataset_name, 'r') as ds_f:
		with open(output_file, 'w') as out_f:
			line_num = 0
			for line in ds_f:
				if line_num == 0:
					protein_name = line.split()[0]
					hssp_file = './msaFiles/{0}.hssp'.format(protein_name)
					# print(hssp_file)
					line_num += 1
				elif (line_num == 1):
					# print(line)
					line_num += 1
					continue
				else:
					labels = (line[:-1]).replace('!', '')
					label_nums = enumerate_cat(labels)
					label_index = 0
					# print(labels)
					try:
						with open(hssp_file, 'r') as hssp_f:
							buf = get_zero_lines(ADD_AMINO_ACIDS)
							buf_len = ADD_AMINO_ACIDS
							amino_count = 0
							for msa_line in hssp_f:
								if (buf_len > 2 * ADD_AMINO_ACIDS):
									temp = buf.replace('\n', '') + label_nums[amino_count]
									out_f.write(temp)
									out_f.write('\n')
									buf = buf.split("\n", 1)[-1]
									buf_len -= 1
									amino_count += 1

								modif_line = (msa_line[:-1]).replace(' ', ',')
								buf = '{0}{1}\n'.format(buf, modif_line)
								buf_len += 1
								

							for i in range(0, ADD_AMINO_ACIDS+1):
								temp = buf.replace('\n', '') + label_nums[amino_count]
								out_f.write(temp)
								out_f.write('\n')
								buf = buf.split("\n", 1)[-1]
								buf = buf + get_zero_lines(1)
								amino_count += 1
							assert amino_count == len(label_nums)
					except Exception:
						print(protein_name)
					line_num = 0
	print('Done with {0} file!'.format(dataset_name))
			