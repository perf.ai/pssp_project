[![PSSP](logo/pssp_logo4.png)](https://gitlab.com/perf.ai/pssp_project)

# pssp_project

pssp_project is an attempt to solve the Protein Secondary Structure Prediction (PSSP) problem using Convolutional Neural Networks and the Hessian Free Optimisation algorithm.

> The project was started in 2019 by Panayiotis Leontiou as a thesis dissertation project, which was part of his Computer Science studies in the University of Cyprus. 

[![License](http://img.shields.io/:license-GNU-green.svg)](https://gitlab.com/perf.ai/pssp_project/-/blob/master/LICENSE)

# What's new?
* The CB513 and PISCES datasets have been [updated](https://gitlab.com/perf.ai/pssp_project/-/tree/master/corrected_datasets), there are no missing or corrupted protein files.
* The v2 version of the [notebook](https://gitlab.com/perf.ai/pssp_project/-/blob/master/Notebooks/CNN_HFO_v2.ipynb) is now available, which uses the corrected datasets.
* A new dataset was added, [CASP13](https://gitlab.com/perf.ai/pssp_project/-/tree/master/corrected_datasets/CASP13), which is used as an independent test dataset.
* You can now use a [script](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/build_dir_tree.sh) to automatically build the directory tree for the filtering methods, instead of doing it manually.

# Table of Contents

- [Installation](#installation)
- [Usage](#usage)
    - [Get notebook and data](#get-notebook-and-data)
    - [Arguments](#arguments)
    - [Filtering techniques](#filtering-techniques)
- [Bugs](#bugs)
- [License](#license)
- [Citation](#citation)

# Installation
It is highly recommended to use Colaboratory ([Colab](https://colab.research.google.com/notebooks/welcome.ipynb)) to run the notebooks, because it allows to write and execute Python code in a browser with:
* Zero configuration required
* Free access to GPUs and TPUs
* Most libraries pre-installed
* Only one requirement, a google account
* Most common Machine Learning frameworks pre-installed and ready to use

### Dependencies

pssp_project requires:

- Python (>= 3.6)
- Jupyter (>= 5.2.2)
- NumPy (>= 1.13.3)
- TensorFlow (>= 2.2.0)
- SciPy (>= 0.19.1)
- Pandas (>= 1.0.3)
- hdf5storage (>= 0.1.15)
- Scikit-learn (>= 0.23.0)

### Installing Jupyter Notebook 
First, ensure that you have the latest pip; older versions may have trouble with some dependencies:
```sh
pip3 install --upgrade pip
```

#### conda (Windows/MacOS/Linux)
If you use conda, you can install Jupyter Notebook with:
```sh
conda install -c conda-forge notebook
```

#### pip (Windows/MacOS/Linux)
If you use pip, you can install Jupyter Notebook with:
```sh
pip install notebook
```

#### run
To run the notebook, use the following command at the Terminal (Mac/Linux) or Command Prompt (Windows):
```sh
jupyter notebook
```

### Installing Scikit-learn 

#### conda (Windows/MacOS/Linux)
If you use conda, you can install it with:
```sh
conda install scikit-learn
```

In order to check your installation you can use:
```sh
conda list scikit-learn # to see which scikit-learn version is installed
conda list # to see all packages installed in the active conda environment
python -c "import sklearn; sklearn.show_versions()"
```

#### pip (Windows/MacOS)
If you use pip, you can install it with:
```sh
pip install -U scikit-learn
```

In order to check your installation you can use:
```sh
python -m pip show scikit-learn # to see which version and where scikit-learn is installed
python -m pip freeze # to see all packages installed in the active virtualenv
python -c "import sklearn; sklearn.show_versions()"
```

# Usage

### Get notebook and data
To perform experiments with PSSP datasets, follow the steps below:

0. Download the [CNN\_HFO\_v2.ipynb](https://gitlab.com/perf.ai/pssp_project/-/blob/master/Notebooks/CNN_HFO_v2.ipynb) notebook and move to step **7**, if you want to use the exact same datasets with _plus7_ neighbouring amino acids. If you prefer to try different datasets or to prepare the datasets yourself, based on the original datasets, continue to step **1**.

1. Download the datasets: [CASP13](https://gitlab.com/perf.ai/pssp_project/-/blob/master/corrected_datasets/CASP13/casp13_sorted.txt), [CB513](https://gitlab.com/perf.ai/pssp_project/-/blob/master/corrected_datasets/CB513/cb513_sorted.txt) and [PISCES](https://gitlab.com/perf.ai/pssp_project/-/blob/master/corrected_datasets/PISCES/pisces_sorted.txt) and MSA profiles: [for CASP13](https://gitlab.com/perf.ai/pssp_project/-/tree/master/corrected_datasets/CASP13/msaFiles_casp13), [for CB513](https://gitlab.com/perf.ai/pssp_project/-/tree/master/corrected_datasets/CB513/msaFiles_cb513) and [for PISCES](https://gitlab.com/perf.ai/pssp_project/-/tree/master/corrected_datasets/PISCES/msaFiles_pisces).

2. Set the "ADD\_AMINO\_ACIDS" variable in [create\_cb513\_ds.py](https://gitlab.com/perf.ai/pssp_project/-/blob/master/originalData_CB513/create_cb513_ds.py) and [create\_pisces\_ds.py](https://gitlab.com/perf.ai/pssp_project/-/blob/master/originalData_PISCES/create_pisces_ds.py) accordingly (> 0). The default value is 7, which will produce new datasets with 7 (_plus7_) neighbouring amino acids (7 + 1 + 7 = 15 amino acids per record).

3. To create the datasets for CB513, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows):
```
python create_cb513_ds.py
```
4. To create the datasets for PISCES, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows):
```
python create_pisces_ds.py
```
5. The current version of the Convolutional Neural Network (CNN) supports only matlab datasets. To convert the datasets to matlab files the [datasets2mat.sh](https://gitlab.com/perf.ai/pssp_project/-/blob/master/datasets2mat.sh) bash script can be used, which converts all ".txt" files of the current directory to ".mat" files. To convert the datasets, run the following commands at the Terminal (Mac/Linux) or Command Prompt (Windows):
```
cd <path_to_dataset> # e.g. cd plus7_CB513
bash <path_to_script> # e.g. bash ../datasets2mat.sh 
```
6. Upload the ".mat" datasets to an online repository (like GitLab or GitHub), which lets you download the uploaded files through a link.

7. Open [Colab](https://colab.research.google.com/notebooks/welcome.ipynb) and sign in to your Google account. If you do not have a Google account, you can create one [here](https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp).

8. Go to _File > Upload notebook > Choose file_ and browse to find the downloaded notebook file [CNN\_HFO\_v2.ipynb](https://gitlab.com/perf.ai/pssp_project/-/blob/master/Notebooks/CNN_HFO_v2.ipynb) If you have already uploaded the notebook to Colab you can open it with _File > Open notebook_ and choose **CNN\_HFO\_v2.ipynb**. 

9. Once the notebook is loaded, go to _Runtime > Change runtime type_ and from the dropdown menu, under **Hardware accelerator**, choose **GPU** and click **Save**.

10. Now you can begin the experiments with the PSSP datasets. All you have to do is to set the **plus_var** (default = 7), choose the **ds_num** (dataset fold number) and the **dataset** ("CB513" or "PISCES"). 
> Note: If you want to use your own datasets you will have to add the URLs in the notebook so that the program can download them or upload them manually in the notebook environment. Under the _Get data_ section you can add your own links for the datasets and set the file names for each dataset. Under the _Predict_ section you will also have to add the links for the original datasets.

11. To train the model go to _Runtime > Run all_ or click on the first cell and use **Shift + Enter** to execute each cell one by one.

12. The hyper parameters of the model can be modified in the cell under **Set Train Arguments** section. 

## Train
Instead of feeding the entire subsampled data into GPU memory to evaluate sub-sampled Gauss-Newton matrix vector product, the samples are divided into segments of size bsize and accumulate results to avoid the out-of-memory issue. For the core operation of Gauss-Newton matrix-vector products, the Tensorflow's vector-Jacobian products are used; the implementation details can be found in this [document](https://www.csie.ntu.edu.tw/~cjlin/papers/cnn/Calculating_Gauss_Newton_Matrix_Vector_product_by_Vector_Jacobian_Products.pdf).

The program gets the validation accuracy at each iteration and returns the best model, if a validation set is provided. Otherwise, the model obtained at the last iteration is returned. The read_data function under the **Utilities** section reads the MATLAB file, performs data normalization and reshapes the input data. Please make sure the input datasets are converted into MATLAB format before they are fed to the network.

### Arguments
Available options and default hyper parameters:

#### General 
1. --optim: the optimization method used for training CNN. (NewtonCG or SGD)
```
Default: --optim NewtonCG
```

2. --net: network configuration (CNN\_4layers, CNN\_7layers, VGG11, VGG13, VGG16, and VGG19)
```
Default: --net CNN_4layers
```

3. --train\_set & --val\_set: provide the address of .mat file for training or validation (optional).
```
Default: None
```

4. --model: save model to a file
```
Default: --model ./saved_model/model.ckpt
```

5. --loss: which loss function to use: MSELoss or CrossEntropy
```
Default: --loss MSELoss
```

6. --bsize: Split data into segments of size bsize so that each segment can fit into memory for evaluating Gv, stochastic gradient and global gardient. If you encounter Out of Memory (OOM) during training, you may decrease the --bsize parameter to an appropriate value.
```
Default: --bsize 1024
```

7. --log: saving log to a file
```
Default: --log ./running_log/logger.log
```

8. --screen\_log\_only: if specified, log printed on screen only but not stored to the log file
```
Default: --screen_log_only
```

9. --C: regularization parameter. Regularization term = 1/(2C × num\_data) × L2\_norm(weight)^2
```
Default: --C 0.01
```

10. --dim: input dimension of data. Shape must be: height width num_channels
```
Default: --dim 32 32 3
```

11. --seed: specify random seed to make results deterministic. If no random seeds are given, a different result is produced after each run.

#### Newton Method
1. --GNsize: number of samples used in the subsampled Gauss-Newton matrix.
```
Default: --GNsize 4096
```

2. --iter_max: the maximal number of Newton iterations
```
Default: --iter_max 100
```

3. --xi: the tolerance in the relative stopping condition for CG
```
Default: --xi 0.1
```

4. --drop: the drop constants for the LM method
```
Default: --drop 2/3
```

5. --boost: the boost constants for the LM method
```
Default: --boost 3/2
```

6. --eta: the parameter for the line search stopping condition
```
Default: --eta 0.0001
```

7. --CGmax: the maximal number of CG iterations
```
Default: --CGmax 250
```

8. --lambda: the initial lambda for the LM method
```
Default: --lambda 1
```

#### SGD
1. --decay: learning rate decay over each mini-batch update.
```
Default: --decay 0
```

2. --momentum: SGD + momentum
```
Default: --momentum 0
```

3. --epoch_max: number of training epoch
```
Default: --epoch_max 500
```

4. --lr: learning rate
```
Default: --lr 0.01
```

## Test 
### Arguments 
Available options and default hyper parameters:

1. --model: address of the saved model from training
```
Default: --model ./saved_model/model.ckpt
```

2. --dim: input dimension of data. Shape must be: height width num_channels
```
Default: --dim 32 32 3
```

3. --train\_set & --test\_set: provide the directory of .mat file for train and test to get predictions.
```
Default: None
```

4. --bsize: Split data into segments of size bsize so that each segment can fit into memory for stochastic gradient and global gradient. If you encounter Out of Memory (OOM) during training, you may decrease the --bsize parameter to an appropriate value.
```
Default: --bsize 1024
```

5. --loss: which loss function to use: MSELoss or CrossEntropy
```
Default: --loss MSELoss
```

## Filtering techniques
### How to apply ensembles and filtering techniques automatically:
You can either prepare the directory tree manually or download the one used in this dissertation project.

1. If you download the [directory tree](https://gitlab.com/perf.ai/pssp_project/-/blob/master/directory_tree_examples/all_tree_for_article.txt) used [for the article](https://gitlab.com/perf.ai/pssp_project/-/tree/master/post_training_article_results), you can just replace the results of the CNN models with your own results or use the same results. 
> Note: The names of the .txt files can be chosen arbitrarily, however, the names of the directories (folders) should not be modified.

2. To create the directory tree: 
- Manually: check one of the directory tree examples: [CB513 only](https://gitlab.com/perf.ai/pssp_project/-/blob/master/directory_tree_examples/cb513_tree_for_article.txt), [PISCES only](https://gitlab.com/perf.ai/pssp_project/-/blob/master/directory_tree_examples/pisces_tree_for_article.txt), [both CB513 and PISCES](https://gitlab.com/perf.ai/pssp_project/-/blob/master/directory_tree_examples/build_dir_tree.txt) 
- Automatically: download the [build\_dir\_tree](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/build_dir_tree.sh) script and execute one of the following commands in a Terminal (Mac/Linux) or Command Prompt (Windows), to create the directory tree under the current directory.
```sh
bash build_dir_tree.sh CB513 # Create directories for CB513 only
bash build_dir_tree.sh PISCES # Create directories for PISCES only
bash build_dir_tree.sh ALL # Create directories for CB513 and PISCES
```
> Note: You must download the [Q3 and SOV scripts](https://gitlab.com/perf.ai/pssp_project/-/tree/master/post_training_results/q3_sov_scripts).

3. If you just want to check the results of this project you can skip Step 4. You should get the same results anyway, if you have not changed the files you downloaded in Step **1**. 

4. Once the directory setup is ready, you can apply filtering techniques using [CB513_runAll.sh](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/CB513_runAll.sh) script for CB513 or [PISCES_runAll.sh](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/PISCES_runAll.sh) for PISCES. To apply the filtering techniques on the independent dataset (CASP13) predictions you can use [CASP13_runAll.sh](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/CASP13_runAll.sh).
```sh
# Commands for CB513
bash CB513_runAll.sh # apply ensembles and filtering techniques and display CB513 results on screen
bash CB513_runAll.sh > final_results_CB513.txt # apply ensembles and filtering techniques and save CB513 results in "final_results_CB513.txt"
bash CB513_runAll.sh | tee final_results_CB513.txt # apply ensembles and filtering techniques, save CB513 results in "final_results_CB513.txt" and print them on screen
bash CASP13_runAll.sh CB513 | tee final_results_CASP13_for_CB513.txt # apply ensembles and filtering techniques, save CASP13 results for CB513 in "final_results_CASP13_for_CB513.txt" and print them on screen

# Commands for PISCES
bash PISCES_runAll.sh # apply ensembles and filtering techniques and display PISCES results on screen
bash PISCES_runAll.sh > final_results_PISCES.txt # apply ensembles and filtering techniques and save PISCES results in "final_results_PISCES.txt" file
bash PISCES_runAll.sh | tee final_results_PISCES.txt # apply ensembles and filtering techniques, save PISCES results in "final_results_PISCES.txt" and print them on screen
bash CASP13_runAll.sh PISCES | tee final_results_CASP13_for_PISCES.txt # apply ensembles and filtering techniques, save CASP13 results for PISCES in "final_results_CASP13_for_PISCES.txt" and print them on screen
```

5. To view the results in a table-like format you can execute [CB513\_view\_results.sh](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/CB513_view_results.sh) script for CB513 or [PISCES\_view\_results.sh](https://gitlab.com/perf.ai/pssp_project/-/blob/master/post_training_article_results/PISCES_view_results.sh) for PISCES, respectively. 
> Note: To be able to use this script you must save the output from `CB513_runAll.sh` and `PISCES_runAll.sh` to a file. If you choose a custom name for the file, you should pass the path to file as an argument. To view the CASP13 results you can use either `CB513_view_results.sh` or `PISCES_view_results.sh`, for models trained with CB513 and PISCES, respectively. 
```sh
bash CB513_view_results.sh # display results from final_results_CB513.txt file
bash CB513_view_results.sh path/to/file.txt # display results from path/to/file.txt file

bash PISCES_view_results.sh # display results from final_results_PISCES.txt file
bash PISCES_view_results.sh path/to/file.txt # display results path/to/file.txt file
```

### How to apply ensembles and filtering techniques manually:
This approach is not recommended for multiple experiments because it requires significant effort and time, compared to the automatic approach. 

##### Get Q3 and SOV scores for cross validation
To get the overall Q3 accuracy and SOV score, as well as, the Q3 accuracy and SOV score for each class (H, E, C), follow the steps below.
> Note: The names of the files below were chosen arbitrarily, you can use any names you want. The parameters <...> are just placeholders that must be replaced with a string value.

1. First create a .txt file, which includes the path to the file with the prediction results (for test dataset). To do that you can run the following command in the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
echo <prediction_filename> > <filename> # where <prediction_filename> is the name of the file with the predictions
echo "test_prediction.txt" > filename.txt # e.g. for a prediction file named "test_prediction.txt"
```

2. Execute the ensembles.py python program with the following command, in a Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python ensembles.py <filename> <WINDOW> 1 <out_predictions> <out_sov> <out_weka> # where <WINDOW> is the size of the input window used for training the models (default=15)
python ensembles.py filename.txt 15 1 ens_pred.txt ens_sov.txt ens_weka.txt # e.g. for a model with window size of 15 
```

3. Compile and execute the SOV program, using Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
# Compile C programs (if you do not have the sov and runSOV executables)
gcc sov.c -o sov 
gcc runSOV.c -o runSOV  

./runSOV <out_sov> # where <out_sov> is the output file from the ensembles.py program, for SOV
./runSOV ens_sov.txt # e.g. for a sov output file named "ens_sov.txt"
```

4. To display SOV score, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
cat ./resultSOV.txt | grep -e 'SOV' | awk -F' ' '{sovAll += $3; sovH += $4; sovE += $5; sovC += $6} END {printf "\n    SOV_ALL    SOV_H      SOV_E      SOV_C\n    %.4f    %.4f    %.4f    %.4f\n", sovAll/NR, sovH/NR, sovE/NR, sovC/NR}'
```

5. To display Q3 accuracy, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python calc_Q3.py <out_predictions> # where <out_predictions> is the file generated by ensembles.py, for the predictions
python calc_Q3.py ens_pred.txt # e.g. for a prediction file named "ens_pred.txt"
```

##### Get Q3 and SOV scores for ensembles method
To get the overall Q3 accuracy and SOV score, as well as, the Q3 accuracy and SOV score for each class (H, E, C) for the ensembles model, follow the steps below.
> Note: The names of the files below were chosen arbitrarily, you can use any names you want. The parameters <...> are just placeholders that must be replaced with a string value.

1. Create a .txt file with all the filenames with the models' prediction results (for test dataset).
2. Execute the ensembles.py python program with the following command, in a Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python ensembles.py <filenames> <WINDOW> 1 <ens_predictions> <ens_sov> <ens_weka> # where <WINDOW> is the size of the input window used for training the models (default=15)
python ensembles.py filenames.txt 15 1 ensembles_pred.txt ensembles_sov.txt ens_weka.txt # e.g. for a model with window size of 15 
```

3. Compile and execute the SOV program, using Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
# Compile C programs (if you do not have the sov and runSOV executables)
gcc sov.c -o sov 
gcc runSOV.c -o runSOV  

./runSOV <ens_sov> # where <ens_sov> is the output file from the ensembles.py program, for SOV
./runSOV ensembles_sov.txt # e.g. for a sov output file named "ensembles_sov.txt"
```

4. To display SOV score, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
cat ./resultSOV.txt | grep -e 'SOV' | awk -F' ' '{sovAll += $3; sovH += $4; sovE += $5; sovC += $6} END {printf "\n    SOV_ALL    SOV_H      SOV_E      SOV_C\n    %.4f    %.4f    %.4f    %.4f\n", sovAll/NR, sovH/NR, sovE/NR, sovC/NR}'
```

5. To display Q3 accuracy, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python calc_Q3.py <ens_predictions> # where <ens_predictions> is the file generated by ensembles.py, for the predictions
python calc_Q3.py ensembles_pred.txt # e.g. for a prediction file named "ensembles_pred.txt"
```

##### Apply external rules filtering
To get the overall Q3 accuracy and SOV score, as well as, the Q3 accuracy and SOV score for each class (H, E, C) after applying the external rules filtering, follow the steps below.
> Note: The names of the files below were chosen arbitrarily, you can use any names you want. The parameters <...> are just placeholders that must be replaced with a string value.

1. Run the externalRules.py python program with the following command, in the Terminal (Mac/Linux) or Command Prompt (Windows):  
```sh
python externalRules.py <input_predictions> <ext_rules_sov> <ext_rules_predictions> # where <input_predictions> is the predictions file (for test dataset)
python externalRules.py ensembles_pred.txt ens_rules_sov.txt ens_rules_pred.txt # e.g. for using ensembles predictions as input
```

2. Compile and execute the SOV program, using Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
# Compile C programs (if you do not have the sov and runSOV executables)
gcc sov.c -o sov 
gcc runSOV.c -o runSOV  

./runSOV <ext_rules_sov> # where <ext_rules_sov> is the output file from the externalRules.py program, for SOV
./runSOV ens_rules_sov.txt # e.g. for a sov output file named "ens_rules_sov.txt"
```

3. To display SOV score, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
cat ./resultSOV.txt | grep -e 'SOV' | awk -F' ' '{sovAll += $3; sovH += $4; sovE += $5; sovC += $6} END {printf "\n    SOV_ALL    SOV_H      SOV_E      SOV_C\n    %.4f    %.4f    %.4f    %.4f\n", sovAll/NR, sovH/NR, sovE/NR, sovC/NR}'
```

4. To display Q3 accuracy, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python calc_Q3.py <ext_rules_predictions> # where <ext_rules_predictions> is the file generated by externalRules.py, for the predictions
python calc_Q3.py ens_rules_pred.txt # e.g. for a prediction file named "ens_rules_pred.txt"
```

##### Apply SVM, Decision Tree or Random Forest filtering
To get the overall Q3 accuracy and SOV score, as well as, the Q3 accuracy and SOV score for each class (H, E, C) after applying a filtering method, follow the steps below.
> Note: The names of the files below were chosen arbitrarily, you can use any names you want. The parameters <...> are just placeholders that must be replaced with a string value.

1. In order to prepare the datasets for training the filtering model run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python prepare_SVM_files.py <input_predictions> <train_pred_folder>/<train_predictions> <FILTER_WINDOW> <filter_testset> <filter_trainset> 
python prepare_SVM_files.py ensembles_pred.txt CB513_train_pred/fold0_train_pred.txt 13 filter_testset.txt filter_trainset.txt # e.g. for fold 0 of CB513 dataset, using the ensemble model predictions
```

2. To apply the filter run the train_SVM.py python program with the following command, using the Terminal (Mac/Linux) or Command Prompt (Windows):
```sh
python train_SVM.py <filter_testset> <filter_trainset> <FILTER_WINDOW> <input_predictions> <filter_predictions> <filter_sov> <FILTER_OPT> # where <FILTER_OPT> is an integer (1: SVM, 2: Decision Tree, 3: Random Forest) that represents the selected filtering method
python train_SVM.py filter_testset.txt filter_trainset.txt 13 ensembles_pred.txt svm_pred.txt svm_sov.txt 1 # e.g. for SVM filtering, using the ensemble model predictions and a window of size 13
```

3. Compile and execute the SOV program, using Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
# Compile C programs (if you do not have the sov and runSOV executables)
gcc sov.c -o sov 
gcc runSOV.c -o runSOV  

./runSOV <filter_sov> # where <filter_sov> is the output file from the train_SVM.py program, for SOV
./runSOV svm_sov.txt # e.g. for a sov output file named "svm_sov.txt"
```

4. To display SOV score, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
cat ./resultSOV.txt | grep -e 'SOV' | awk -F' ' '{sovAll += $3; sovH += $4; sovE += $5; sovC += $6} END {printf "\n    SOV_ALL    SOV_H      SOV_E      SOV_C\n    %.4f    %.4f    %.4f    %.4f\n", sovAll/NR, sovH/NR, sovE/NR, sovC/NR}'
```

5. To display Q3 accuracy, run the following command at the Terminal (Mac/Linux) or Command Prompt (Windows): 
```sh
python calc_Q3.py <filter_predictions> # where <filter_predictions> is the file generated by train_SVM.py, for the filtered predictions
python calc_Q3.py svm_pred.txt # e.g. for a prediction file named "svm_pred.txt"
```

> Note: The ensembles and filtering methods can be applied in any order. It seems that this order affects the final results, so try various combinations and select the one with the best results.

# Bugs
No know bugs

# License
[![License](http://img.shields.io/:license-GNU-green.svg)](https://gitlab.com/perf.ai/pssp_project/-/blob/master/LICENSE)

- This project is under [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).
- Copyright 2020 © [Panayiotis Leontiou](https://www.linkedin.com/in/panayiotis-leon/)

# Citation

If you want to use any of these in your research project you can use the following citation: 
> Panayiotis Leontiou, Protein Secondary Structure Prediction Using Convolutional Neural Networks and Hessian Free Optimisation, BSc Thesis, Department of Computer Science, University of Cyprus, 2020.

If you are using Bibtex:
```
@MISC{leontiou_2020,
    author = "Leontiou, Panayiotis",
    title = "Protein Secondary Structure Prediction Using Convolutional Neural Networks and Hessian Free Optimisation",
    note = "BSc Thesis, Department of Computer Science, University of Cyprus",
    year = "2020",
}
```
... and in order to cite it anywhere in the document: `\cite{leontiou_2020}`