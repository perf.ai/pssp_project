import re
import sys
import numpy as np


class afterSOV:
    def run(SOVFile, output):
        f = open(SOVFile, "r")
        lines = f.readlines()
        f.close()
        sov = list()
        q3 = list()
        for s in lines:
            if 'SOV' in s:
                sov.append(list(map(float, re.findall(r'\d+\.\d+', s))))
            elif 'Q3' in s:
                q3.append(list(map(float, re.findall(r'\d+\.\d+', s))))
        sov = np.asarray(sov)
        q3 = np.asarray(q3)
        sov = sum(sov) / len(sov)
        q3 = sum(q3) / len(q3)
        print('{0:10}{1:10}{2:10}{3:10}'.format(' SOVall', ' SOVH', ' SOVE', ' SOVC'))
        print('{0:10f}{1:10f}{2:10f}{3:10f}\n'.format(sov[0], sov[1], sov[2], sov[3]))
        print('{0:10}{1:10}{2:10}{3:10}'.format(' Q3all', ' Q3H', ' Q3E', ' Q3C'))
        print('{0:10f}{1:10f}{2:10f}{3:10f}'.format(q3[0], q3[1], q3[2], q3[3]))
        f = open(output, "w")
        f.write('{0:10}{1:10}{2:10}{3:10}\n'.format(' SOVall', ' SOVH', ' SOVE', ' SOVC'))
        f.write('{0:10f}{1:10f}{2:10f}{3:10f}\n'.format(sov[0], sov[1], sov[2], sov[3]))
        f.write('{0:10}{1:10}{2:10}{3:10}\n'.format(' Q3all', ' Q3H', ' Q3E', ' Q3C'))
        f.write('{0:10f}{1:10f}{2:10f}{3:10f}'.format(q3[0], q3[1], q3[2], q3[3]))
        f.close()

    run(sys.argv[1].replace(',', ''), sys.argv[2].replace(',', ''))
    # print('\nEnd of after SOV processing script\n')
