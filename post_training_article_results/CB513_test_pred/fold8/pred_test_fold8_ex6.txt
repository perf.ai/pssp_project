> 1ATPI_5-24
TTYADFIASGRTGRRNAIHD
CHHHHHHCCCCCCCCCCCCC
CCHEEEEHCCCCCCHHHHCC
> 2MHUA_1-30
MDPNCSCAAGDSCTCAGSCKCKECKCTSCK
CCCCCCCCCCCCCCCCCCCCCCCCCCHHHC
CCCCCCECCCCCCCCCCCCCCHCCCCCCCC
> 1CRNA_1-46
TTCCPSIVARSNFNVCRLPGTPEAICATYTGCIIIPGATCPGDYAN
CEECCCHHHHHHHHHHHCCCCCHHHHHHHHCCEECCCCCCCHHHCC
CCCCCCCCCHHHHHHEECCCCCCHHHHCCCCCEEECCCCCCCCCCC
> 1BNCB_131-205
VPGSDGDDMDKNRAIAKRIGYPVIIKRVVRGDAELAQSISMTRAYMEKYLE
CCECCCCCCCHHHHHHHHHCCCEEECCEECCCCCHHHHHHHHHCCEEECCC
CCCCCCCCHHHHHHHHHHCCCEEEEEEHCCCHHHHHHHHHHHHHHHHHHHC
> 1DURA_1-55
AYVINDSCIACGACKPECPVNCIQEGSIYAIDADSCIDCGSCASVCPVGAPNPED
CEEECCCCCCCCCCHHHCCCCCEECCCCCEECCCCCCCCCHHHHHCCCCCEEECC
CEEECCHCCCCCCCCCCCCCCCEECCCEEEECCCCCCCCCCCCCCCCCCCCCCCC
> 1OVOA_1-56
LAAVSVDCSEYPKPACPKDYRPVCGSDNKTYSNKCNFCNAVVESNGTLTLNHFGKC
CCCEEECCCCCCCCCCCCCCCCEEECCCCEECCHHHHHHHHHHCCCCCCEEEECCC
CCCCCCCCCCCCCCCCCCCCCCECCCCCCECCCHHHHHHHHECCCCCEEEEECCCC
> 1SEIB_74-130
GLKRISKPGLRVYVKAHEVPRVLNGLGIAILSTSQGVLTDKEARQKGTGGEIIAYVI
CEEECCECCECCEECHHHCCCCCCCCCEEEEEECCEEEEHHHHHHHCCCEEEEEEEC
CCEEECCCCCEEECCHCCCHHHHCCCEEEEEECCCCCECCHHHHHHCCCHHEEEEEC
> 1IL8A_2-72
AKELRCQCIKTYSKPFHPKFIKELRVIESGPHCANTEIIVKLSDGRELCLDPKENWVQRVVEKFLKRAENS
CCCCCCCCCCCCCCCCCHHHEEEEEEECCCCCCCCCEEEEEECCCEEEEECCCCHHHHHHHHHHHHHHHHC
CCCCEEEEECCCCCCCCCHHHEEEEEECCCCCCCCCEEEEEECCCCEEECCCCHHHHHHHHHHHHHHCCCC
> 1AVHB_247-320
IPAYLAETLYYAMKGAGTDDHTLIRVMVSRSEIDLFNIRKEFRKNFATSLYSMIKGDTSGDYKKALLLLCGEDD
HHHHHHHHHHHCCCCCCCCHHHHHHHHHHCCCCCHHHHHHHHHHHHCCCHHHHHHHHCCCHHHHHHHHHHCCCC
CCHHHHHHHHHHHCCCCCCCHEEEEEEECCCHHHHHHHHHHHHHHCCCCHHHHHHCCCCCCHHHHHHHHCCCCC
> 3AITA_1-74
DTTVSEPAPSCVTLYQSWRYSQADNGCAETVTVKVVYEDDTEGLCYAVAPGQITTVGDGYIGSHGHARYLARCL
CCCCCCECCCCEEEEECCCCEEEEECCCCCEEEEEEECCCCEEEEEEECCCCEEEEECCCCECCECEEEEEECC
CCCCCCCCCCCEEEEHCEEEEECCCCCCCEEEEEEEECCCCCCCEEECCCCCEEECCCCEECCCCCHHHHHHHC
> 1OACB_5-86
AHMVPMDKTLKEFGADVQWDDYAQLFTLIKDGAYVKVKPGAQTAIVNGQPLALQVPVVMKDNKAWVSDTFINDVFQSGLDQT
CCEEEHHHHHHHHCCEEEEECCCCEEEEEECCEEEEECCCCCEEEECCEEEECCCCCEECCCCEEEECCHHHHHCCCCCCCC
CCEEHHHHHHHHCCCCEEECCCCCEEEEECCCCEEEEEECCCEEEECCCEEECCCCEEEECCCEEEEEHHHHHHHCCCCCCC
> 1PPIA_404-496
EPFANWWDNGSNQVAFGRGNRGFIVFNNDDWQLSSTLQTGLPGGTYCDVISGDKVGNSCTGIKVYVSSDGTAQFSISNSAEDPFIAIHAESKL
CCEEEEEECCCCEEEEEECCCEEEEEECCCCCEEEEEECCCCCEEEECCCCCCEECCEECCCEEEECCCCEEEEEECCCCCCCEEEEEHHHEC
CCCCCCCCCCCCEEEEECCCCCEEEEECCCCCCCHHEEECCCCCCEEEEECCCCCCCCCCCEEEEECCCCEEEEEECCCCCCHEEEEEHCCCC
> 1PDAA_99-200
CEREDPRDAFVSNNYDSLDALPAGSIVGTSSLRRQCQLAERRPDLIIRSLRGNVGTRLSKLDNGEYDAIILAVAGLKRLGLESRIRAALPPEISLPAVGQGA
CCCCCCCEEEECCCCCCCCCCCCCCEEECCCHHHHHHHHHHCCCCEEECCCCCHHHHHHHHHCCCCCEEEEEHHHHHHCCCHHHCCEECCCCCCCCCCCCCC
CCCCCCCHEEEECCCCCHHHCCCCCEECCCCHHHHHHHHHHCCCCEEEEECCCHHHHHHHHHCCCHHHHHHHHHHHHHCCCHCCHEEECCCCCCCCCCCCCC
> 1HIWS_7-109
VLSGGELDKWEKIRLRPGGKKQYKLKHIVWASRELERFAVNPGLLETSEGCRQILGQLQPSLQTGSEELRSLYNTIAVLYCVHQRIDVKDTKEALDKIEEEQN
CCCCCHHHHHCCCECCCCCCCECCCHHHHHHHHHHHHCCECHHHCCCHHHHHHHHHHHHHHCCCECHHHHHHHHHHHHHHHHHCCCCCCEHHHHHHHHHHHHC
ECCCCCCCHHHEEEECCCCHHHHEEEEEEEHHHHCHHHHHCCCCEECCCCCHHHHHCCCHHHCCCCHHHHHHHHHHHEHEHHHHHEEECCHHHHHHHHHHHCC
> 1FJMB_49-159
QPILLELEAPLKICGDIHGQYYDLLRLFEYGGFPPESNYLFLGDYVDRGKQSLETICLLLAYKIKYPENFFLLRGNHECASINRIYGFYDECKRRYNIKLWKTFTDCFNCL
CCCEEEECCCEEEECECCCCHHHHHHHHHHHCCCCCCCEEECCCCCCCCCCHHHHHHHHHHHHHHCCCCEEECCCCCCCHHHHHHCCHHHHHHHHCCHHHHHHHHHHHCCC
CCCEEECCCCEEEEECCCCHHHHHHHHHHHCCCCCCCEEEEEECCCCCCCCCCCHHHHHHHHHHHCCCCEEEECCCCCHHHHHHHHCCHHHHHHHHCHHHHHHHHHHHHHC
> 1RLDS_510-623
KKYETLSYLPDLSQEQLLSEVEYLLKNGWVPCLEFETEHGFVYRENNKSPGYYDGRYWTMWKLPMFGCTDATQVLAEVEEAKKAYPQAWIRIIGFDNVRQVQCISFIAYKPEGY
CCCCCCCCCCCCCHHHHHHHHHHHHHHCCEEEEEEECCCCCCECCCCCCCCCCECCCCEEECCCCCCCCCHHHHHHHHHHHHHHCCCCEEEEEEEECCCCEEEEEEEEECCCCC
CCEEEHHHCCCCCHHHHHHHHHHHHHCCCEEEEEECCCCCCEEECCCCCCCCCCCCEEEHCCCCCCCCCCHHHHHHHHHHHHHHCCCCCEEEEEECCCCCEEEEEEEEECCCCC
> 2RSPA_1-124
LAMTMEHKDRPLVRVILTNTGSHPVKQRSVYITALLDSGADITIISEEDWPTDWPVMEAAGIPMRKSRDMIELGVINRDGSLERPLLLFPAVAMVRGSILGRDCLQGLGLRLTNL
CEEECCCCCCCEEEEEEEECCCCCCCCCEEEEEEEECCCCCCCEEECCCCCCCCCEEECCCCCEEEECCCEEEEEECCCCCECCCEEECCEEECCCCEEECHHHHHHCCCEEECC
CEEEEEHCCCCCEEEEECCCCCCCCCCCCCEEEEEECCCCCCEEEECCCCCCCCCCCCECCCCCCCCCCCEEEEEECCCCCCCCCEEEEEEECCCCEEEHHHHHHHHHCCEECCC
> 1DAAB_1-119
GYTLWNDQIVKDEEVKIDKEDRGYQFGDGVYEVVKVYNGEMFTVNEHIDRLYASAEKIRITIPYTKDKFHQLLHELVEKNELNTGHIYFQVTRGTSPRAHQFPENTVKPVIIGYTKENP
CEEEECCEEEEHHHCCECCCEHHHHCCCEEEEEEEEECCEECCHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHCCCCEEEEEEEECCECCCCCCCCCCCCCCEEEEEEEECC
CEEEECCCECCCCCCEEEECCCHHHHCCCHEEEEEECCCCEECHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHCCCCCCEEEEEEEECCCCCCCCCCCCCCCCEEEEECCCCC
> 2CCYA_2-128
QSKPEDLLKLRQGLMQTLKSQWVPIAGFAAGKADLPADAAQRAENMAMVAKLAPIGWAKGTEALPNGETKPEAFGSKSAEFLEGWKALATESTKLAAAAKAGPDALKAQAAATGKVCKACHEEFKQD
CCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCECHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHECC
CCCCHHHHHHHHHHHHHHHHHHCHHHHHHCCCCCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHCCCCCCCCCHHHHHHC
> 1TRBA_117-244
RYLGLPSEEAFKGRGVSACATSDGFFYRNQKVAVIGGGNTAVEEALYLSNIASEVHLIHRRDGFRAEKILIKRLMDKVENGNIILHTNRTLEEVTGDQMGVTGVRLRDTQNSDNIESLDVAGLFVAIG
CCCCCHHHHHCECCCEECCHHHHHHHHCCCEEEEECCCHHHHHHHHHHCCCCCEEEEECCCCCCCCCHHHHHHHHHHHHCCCEEEECCCEEEEEEECCCCEEEEEEECCCCCCCCEEEECCEEEECCC
CCCCCCCHHHHCCCCCEEEECCCCCHECCCEEEEECCCCHHHHHHHHHHHHCCEEEEEECCCCCCCCHHHHHHHHHHHHCCCEEEEECCCEEEEECCCCCEEEEEEEECCCCCCEEEEEECEEEEEEC
> 1DIKA_243-372
VFGNKGETSGTGVAFTRNPSTGEKGIYGEYLINAQGEDVVAGVRTPQPITQLENDMPDCYKQFMDLAMKLEKHFRDMQDMEFTIEEGKLYFLQTRNGKRTAPAALQIACDLVDEGMITEEEAVVRIEAKS
CCCCCCCCCEEEEEEEECCCCCCEEEEEEEEECCCHHHHCCCCCCCEECCCHHHHCHHHHHHHHHHHHHHHHHCCCCEEEEEEEECCEEEEEECCECCCCHHHHHHHHHHHHHCCCCCHHHHHHHCCHHH
CCCCCCCCCCCEEEEEECCCCCCCEEEEEEEHCCCCCEEEECCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEEEHCCCEEEEECCCCCCCHHHHHHHHHHHHHHCCCCHHHHEEECCCCC
> 1GTMC_206-339
GWDTLKGKTIAIQGYGNAGYYLAKIMSEDFGMKVVAVSDSKGGIYNPDGLNADEVLKWKNEHGSVKDFPGATNITNEELLELEVDVLAPAAIEEVITKKNADNIKAKIVAEVANGPVTPEADEILFEKGILQIP
CCCCCCCCEEEEECCCHHHHHHHHHHHHHHCCEEEEEEECCEEEECCCCCCHHHHHHHHHHHCCCCCCCCCEEECHHHHCCCCCCEEEECCCCCCECHHHHCCCCCCEEECCCCCCECHHHHHHHHHCCCEEEC
CCCCCCCCEEEEECCCHHHHHHHHHHHHHCCCEEEEEECCCCEEECCCCCCHHHHHHHHHHCCCCCCCCCCCEECCCCCCCCCCCEEEECCCCCCCCHHHHHHHCEEEEECCCCCCCCHHHHHHHHHCCEEECC
> 1FUQB_4-139
VRSEKDSMGAIDVPADKLWGAQTQRSLEHFRISTEKMPTSLIHALALTKRAAAKVNEDLGLLSEEKASAIRQAADEVLAGQHDDEFPLAIWQTGSGTQSNMNMNEVLANRASELLGGVRGMERKVHPNDDVNKSQS
EEEEEECCEEEEEECCCCCCHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHCCCCHHHCCCECCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHCCCCC
CCEEECCCCCEECCCHHHCCCHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEEEEECCCCCCEECCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCC
> 2OLBA_45-180
EGLLISDVEGHPSPGVAEKWENKDFKVWTFHLRENAKWSDGTPVTAHDFVYSWQRLADPNTASPYASYLQYGHIANIDDIIAGKKPATDLGVKALDDHTFEVTLSEPVPYFYKLLVHPSVSPVPKSAVEKFGDKWT
CCCEEECCCCCEEECCEEEEEEECCCEEEEEECCCCECCCCCECCHHHHHHHHHHHHCHHHCCCCCCHHHHCCECCHHHHHCCCCCHHHCCEEEEECCEEEEEECCCCCCHHHHHHCHHHCCCCHHHHHHHHHHCC
CCCEECCCCCCCCCCEEECECCCCCCEEEEEECCCCEECCCCCCCHHHHHHHHHHHCCCCCCCCHHHHHCCCCHHCHHHHHCCCCCCCCCEEEECCCCEEEEEECCCCCHHHHHHCCCCCCCCCHHHHHHCCCCCC
> 2FOXA_1-138
MKIVYWSGTGNTEKMAELIAKGIIESGKDVNTINVSDVNIDELLNEDILILGCSAMGDEVLEESEFEPFIEEISTKISGKKVALFGSYGWGDGKWMRDFEERMNGYGCVVVETPLIVQNEPDEAEQDCIEFGKKIANI
CEEEEECCCCHHHHHHHHHHHHHHHCCCCCEEEEHHHCCHHHHCCCCEEEEEECCECCCECCCCCHHHHHHHHCCCCCCCEEEEEEEECCCCCHHHHHHHHHHHHCCCEECCCCEEEECCCCCCHHHHHHHHHHHCCC
EEEEEECCCCCHHHHHHHHHHHHHHCCCCEEEEECCCCCHHHHHHHHEEEEEECCCCCCCCCHHHHHHHHHHHCCCCCCCEEEEEEECCCCCCCCHHHHHHHHHHCCCEEECCCEEECCCCCHHHHHHHHHHHHHHHC
> 1STME_17-157
AAATSLVYDTCYVTLTERATTSFQRQSFPTLKGMGDRAFQVVAFTIQGVSAAPLMYNARLYNPGDTDSVHATGVQLMGTVPRTVRLTPRVGQNNWFFGNTEEAETILAIDGLVSTKGANAPSNTVIVTGCFRLAPSELQSS
CCCCEEEEEEEEECCCCCCEEEEEHHHCHHHHHCCCCCEEEEEEEEEEEECCCEEEEEEECCCCCCCCCEECCCEEECCCCEEEEECCCCCCCCCECCCCCCCCEEEEEEEECCCCCCCCCCEEEEEEEEEEECCCCCCCC
CCEHHHHEEEEEEEEEEECECCEEECCCCCCCCCCCCCEEEEEEEEEEECCCHHHHHHHECCCCCCCCCCCCCEEECCCCCCEEECCCCCCCCCEECCCCCHHEEEEEECCCECECCCCCCCHEEEEEEEEEECCCCCCCC
> 1INPA_248-400
QGTQNPSSEGSCRFSVVISTSEKETIKGALSHVCGERIFRAAGAGYKSLCVILGLADIYIFSEDTTFKWDSCAAHAILRAMGGGMVDLKECLERNPDTGLDLPQLVYHVGNEGAAGVDQWANKGGLIAYRSEKQLETFLSRLLQHLAPVATHT
CCCCECCECCCCCCCCEEEECCCCCCCCCCCCCCCCEEEECCCHHHHHHHHHHCCCCEEEECCCCCEHHHHHHHHHHHHCCCCEEEEHHHHHHCCCCCCCCCCECCCCCCCCCCCCCCCCECCCCEEEECCHHHHHHHCCCCCCCCCCCCCCC
CCCCCCCCCCCCCEEEEEECCCCHHHHHHHHHCCCCCEEECCCCHHHHHHHHHCCCEEEEECCCCCCCCEECCHHHHHHHCCCCEEECCCCCCCCCCCCCCCCCEEECCCCCCCCHHHHHHCCCCEEEECCHHHHHHHHHHHHHCCCCCCCCC
> 6DFRA_1-159
MISLIAALAVDRVIGPWNLPADLAWFKRNTLDKPVIMGRHTWESIGRPLPGRKNIILSSQPGTDDRVTWVKSVDEAIAACGDVPEIMVIGGGRVYEQFLPKAQKLYLTHIDAEVEGDTHFPDYEPDDWESVFSEFHDADAQNSHSYCFEILERR
CEEEEEEECCCCECCCCCCHHHHHHHHHHHCCCCEEEEHHHHHHHCCCCCCCCEEEECCCCCCCCCCEEECCHHHHHHCCCCCCCEEECCCHHHHHHHHHHCCEEEEEEECCCCCCCCECCCCCHHHEEEEEEEEECCCCCCCCCEEEEEEEEC
CHEEEEEECCCCCECCCCCHHHHHHHHHHHCCCCEEECCECHHCCCCCCCCCCEEEEECCCCCCCCCEEECCHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHHEEEEEECCCCCCCCECCCCCCCCCEEECCCCCCCCCCCCCEEEEEEEECC
> 1HANA_134-288
VSGFLTGEQGLGHFVRCVPDSDKALAFYTDVLGFQLSDVIDMKMGPDVTVPAYFLHCNERHHTLAIAAFPLPKRIHHFMLEVASLDDVGFAFDRVDADGLITSTLGRHTNDHMVSFYASTPSGVEVEYGWSARTVDRSWVVVRHDSPSMWGHKSV
CCCECCHHHCCCEEEEECCCHHHHHHHHHHCCCCEEEEEEEEECCCCCEEEEEEEECCCECCCEEEECCCCCCCEEEEEEEECCHHHHHHHHHHHHCCCCEEEEEEEECCCCCEEEEEECCCCCEEEEEECCCECCCCCCCEEECCCEEEECCEC
CCCEECCHCCCCEEEEECCCHHHHHHHHHHHCCCEEHHHEECCCCCCCCCEEEEEECCCCCCEEEEECCCCCCCCEEEEEECCCHHHHHHHHHHHHHCCCEEECCCCCCCCCCEEEEEECCCCCEEEEECCCCCCCCCCCCCCCCCCCCCCCCCC
> 1CYXA_125-282
KPITIEVVSMDWKWFFIYPEQGIATVNEIAFPANTPVYFKVTSNSVMHSFFIPRLGSQIYAMAGMQTRLHLIANEPGTYDGICAEICGPGHSGMKFKAIATPDRAAFDQWVAKAKQSPNTMSDMAAFEKLAAPSEYNQVEYFSNVKPDLFADVINKFM
CCEEEEEEEECCEEEEEECCCCEEEECEEEEECCCCEEEEEEECCCCEEEEEHHHCEEEEECCCCCEEEEECCCCCEEEEEEECCCCCCCCCCCCEEEEEECCHHHHHHHHHHHHCCCCCECCHHHHHHHHCCCCCCCCEEECCECCCHHHHHHCCCC
CCEEEEEEECCCEEEEECCHHCCEEEEEEEECCCCCEEEEEECCCCHHHEECHCHCCEECCCCCCCCEEEEEECCCCCECCCCCCCCCCCCCCCEEEEEECCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHCCCCCCCEEEECCCCCCHHHHHHHHCC
> 1MMOH_4-165
LGIHSNDTRDAWVNKIAHVNTLEKAAEMLKQFRMDHTTPFRNSYELDNDYLWIEAKLEEKVAVLKARAFNEVDFRHKTAFGEDAKSVLDGTVAKMNAAKDKWEAEKIHIGFRQAYKPPIMPVNYFLDGERQLGTRLMELRNLNYYDTPLEELRKQRGVRVVH
CECCECHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCHHHHHHECCCCCEHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCEECC
CCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCEEEEECCCHHHHHHHHHHHCCCCHHCCCHHHHHHHCCEEEEC
> 1TIEA_1-170
VLLDGNGEVVQNGGTYYLLPQVWAQGGGVQLAKTGEETCPLTVVQSPNELSDGKPIRIESRLRSAFIPDDDKVRIGFAYAPKCAPSPWWTVVEGLSVKLSEDESTQFDYPFKFEQVSDQLHSYKLLYCEGKHEKCASIGINRDQKGYRRLVVTEDYPLTVVLKKDE
CCECCCCCECCCCCEEEEEECCHHHCCCEEEECCCCCCCCCEEEECCCCCCCCCCEEEEECCCCCCCCCCCCEEEEECCCCCCCCCCEEEEECCCEEEECCCCHHHHCCCEEEEEEECCCCEEEEEEECCCCCCEEEEEEEECCCCCEEEEECCCCECEEEEEECC
EEEECCCCCCCCCCEEEEEECCCCCCCCEEECCCCCCCCCCEEEECCCCCCCCCEEEECCCCCCCEEECCCEEEEEECCCCCCCCCCEEEEECCEEEEECCCCCCCCCCEEEEEEECCCCCCEEEEECCCCCCCCCCEEEECCCCCCEEEEECCCCCEEEEEEECC
> 1RBPA_1-174
ERDCRVSSFRVKENFDKARFSGTWYAMAKKDPEGLFLQDNIVAEFSVDETGQMSATAKGRVRLLNNWDVCADMVGTFTDTEDPAKFKMKYWGVASFLQKGNDDHWIVDTDYDTYAVQYSCRLLNLDGTCADSYSFVFSRDPNGLPPEAQKIVRQRQEELCLARQYRLIVHNGYC
CCCCCHHHCCCCCCCCCCCCCEEEEEEEEECCCCCCCCEEEEEEEEECCCCCEEEEEEEEEEECCCEEEEEEEEEEEEECCCCCEEEEEEEECCCCCCCEEEEEEEEEECCCCEEEEEEEEEECCCCCEEEEEEEEEECCCCCCCHHHHHHHHHHHHHCCCCCCCEECCCCCCC
CCCCCCCCCCCCCCCCHHHHCCCEEEEEECCCCCHHHCCCCEEEEEECCCCCEEEECCCCEEECCCCCCEEEEEEEECCCCCCCEEEEEECCCCCCCCCCCCCEEEEECCCHHHEEEEECCCCCCCCCCCCHEEEEEECCCCCCCHHHHHHHHHHHHHCCCCCCEEEECCCCCC
> 1DNPB_290-469
SLCKHRPFIAWTDRVQWQSNPAHLQAWQEGKTGYPIVDAAMRQLNSTGWMHNRLRMITASFLVKDLLIDWREGERYFMSQLIDGDLAANNGGWQWAASTGTDAAPYFRIFNPTTQGEKFDHEGEFIRQWLPELRDVPGKVVHEPWKWAQKAGVTLDYPQPIVEHKEARVQTLAAYEAARK
HHHHCCCCCHHHHHCCCCCCHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCCCHHHHHHHHHHHCCCCCHHHHHHHHHHHCCCCCCCCCCCCCCCHHHHHHHHCCCCHHHHHHCHHHCCCCCCCCCCHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHC
CHHHCCCCCCCCCCCCCCCCHHHHHHHHCCCCCCEHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHCCCCHHCCCCCHEECCCCCCCCCCEEECCCHHHHCCCCCCCHEHHHHHHHHCCCCCHHHCCCCCCCHHCCCCCCCCCCEEHHHHHHHHHHHHHHHHHC
> 2BUKA_12-195
TMRAVKRMINTHLEHKRFALINSGNTNATAGTVQNLSNGIIQGDDINQRSGDQVRIVSHKLHVRGTAITVSQTFRFIWFRDNMNRGTTPTVLEVLNTANFMSQYNPITLQQKRFTILKDVTLNCSLTGESIKDRIINLPGQLVNYNGATAVAASNGPGAIFMLQIGDSLVGLWDSSYEAVYTDA
CHHHHHHHHHCCCCEEEEEEEEEEEECCCCCEEEECCCCCCECCCCCCECCCEEEEEEEEEEEEEECCCCCEEEEEEEEEECCCCCCCCCHHHHECCCCCCCCECHHHHHCCCEEEEEEEEEEECCCCCCEEEEEEEEECCEEEECCCCCCHHHECCCEEEEEEEECCCCCEEEEEEEEEEECC
CCHHHHHHHCCCHHHHHHHHCCCCCCEECCCCEEECCCCEEECCCCHCCCCCEEEEHEEEEEEEEECCCCCHCEEEEEECCCCCCCCCCHHEEEECHCCCCCCCCCCHHHHCHEEEEEEHEHEHHHCCCCHHEEEECCCCEEEEECCHHHHHHCCCCCEEEEEEECCCEEECEECCHHHHHECC
> 2NADB_150-334
VAEHVVMMILSLVRNYLPSHEWARKGGWNIADCVSHAYDLEAMHVGTVAAGRIGLAVLRRLAPFDVHLHYTDRHRLPESVEKELNLTWHATREDMYPVCDVVTLNCPLHPETEHMINDETLKLFKRGAYIVNTARGKLCDRDAVARALESGRLAGYAGDVWFPQPAPKDHPWRTMPYNGMTPHIS
HHHHHHHHHHHHHCCHHHHHHHHHCCCCCHHHHHCCCCCCCCCEEEEECCCHHHHHHHHHHHHHCCEEEEECCCCCCHHHHHHHCEEECCCHHHHHHHCCEEEECCCCCCCCCCCECHHHHCCCCCCEEEEECCCHHHECHHHHHHHHHCCCEEEEEECCCCCCCCCCCCHHHCCCCECCCCCCH
CHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCECCCCEEEEEECCHHHHHHHHHHHHCCCEEEEECCCCCCHHHHHCCCCCEECCHHHHHHHCCEEEEECCCCCCCCCCCCHHHHHHCCCCEEEEECCCCCECHHHHHHHHHHHCCCEEEEECECCCCCCCCCCCHHCCCCEEECCCCC
> 2TRTA_2-208
ARLNRESVIDAALELLNETGIDGLTTRKLAQKLGIEQPTLYWHVKNKRALLDALAVEILARHHDYSLPAAGESWQSFLRNNAMSFRRALLRYRDGAKVHLGTRPDEKQYDTVETQLRFMTENGFSLRDGLYAISAVSHFTLGAVLEQQEHTAALNLPPLLREALQIMDSDDGEQAFLHGLESLIRGFEVQLTALLQIV
CCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHCCCHHHHHHHCCCHHHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHCCCCCCHHHHHCCCCCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCC
CHHCHHHHHHHHHHHHHHCCCCCHCHHHHHHHHCCCCCEEEEECCCHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHCCC
> 1NBAC_36-249
KRRIGYGNRPAVIHIDLANAWTQPGHPFSCPGMETIIPNVQRINEAARAKGVPVFYTTNVYRNRDASSGTNDMGLWYSKIPTETLPADSYWAQIDDRIAPADGEVVIEKNRASAFPGTNLELFLTSNRIDTLIVTGATAAGCVRHTVEDAIAKGFRPIIPRETIGDRVPGVVQWNLYDIDNKFGDVESTDSVVQYLDALPQFEDTVPKTLSDPQ
CCCCCCCCCEEEEEECCEHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEECECCCCCCCCCCCCHHHHHCCCHHHCECCCHHHCECHHHCCCCCCEEEEECCCCCCCCCCHHHHHHHCCCCEEEEEEECCCCHHHHHHHHHHHHCCEEEEEHHHEECCCCCHHHHHHHHHHHHCCEEECHHHHHHHHHHCCCHHHCCCCCCCCCC
CCCCCCCCCEEEEEEEHHCHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHCCCHHCCCCCCCEEEECCCCCCCCCCHHHHHHHHCCCCEEEEEEECCCCEHHHHHHHHHHCCCEEEEEECCCCCCCHHHHHHHHHHHHHHCCEEECHHHHHHHHHHHHHHCCCCCCCCCCCC
> 1AOZB_337-552
PKPPVKFNRRIFLLNTQNVINGYVKWAINDVSLALPPTPYLGAMKYNLLHAFDQNPPPEVFPEDYDIDTPPTNEKTRIGNGVYQFKIGEVVDVILQNANMMKENLSETHPWHLHGHDFWVLGYGDGKFSAEEESSLNLKNPPLRNTVVIFPYGWTAIRFVADNPGVWAFHCHIEPHLHMGMGVVFAEGVEKVGRIPTKALACGGTAKSLINNPKNP
CCCCCCCCEEEEEEEEEEEECCEEEEEECCEEECCCCCCHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCEEECCCCEEEEEEECCCCCCCCCCCCEEEEECCCCEEEEEEEECCCCHHHHHHCCCCCCCEECEEEECCCEEEEEEEECCCCEEEEEEECCHHHHHCCCEEEEEECHHHCCCCCHHHHCCHHHHHHHCCCCCCC
CCCCCCCCEEEEEEECCCCCCCCEEEEECCCEEECCCCHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCEEEEEEECCCCCCCCCCCCCCEEECCCCEEEEECCCCCCCCCCCCCCCCCCCCCCEEEEECCCCEEEEEEECCCCCCEEEHHHHHHHHHHCCEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCC
> 1MNSA_121-348
TPLVKLLGANARPVQAYDSHSLDGVKLATERAVTAAELGFRAVKTKIGYPALDQDLAVVRSIRQAVGDDFGIMVDYNQSLDVPAAIKRSQALQQEGVTWIEEPTLQHDYEGHQRIQSKLNVPVQMGENWLGPEEMFKALSIGACRLAMPDAMKIGGVTGWIRASALAQQFGIPMSSHLFQEISAHLLAATPTAHWLERLDLAGSVIEPTLTFEGGNAVIPDLPGVGII
CEHHHHCCCCCCCEEEEEECCCCHHHHHHHHHHHHHHCCCCEEEEECCCCCHHHHHHHHHHHHHHHCCCCEEEEECCCCCCHHHHHHHHHHHHHHCCCEEECCCCCCCHHHHHHHHHCCCCCEEECCCCCCHHHHHHHHHCCCCCEECCECCCCCHHHHHHHHHHHHHHHCCCECCECCHHHHHHHHHCCCCECCEEECCCCCCCECCCCEEECCEEECCCCCECCCC
CCHHHHCCCCCCCCEEEEECCCCCHHHHHHHHHHHHHHCCEEEEEECCCCCHHHHHHHHHHHHHHCCCCEEEEEECCCCCCHHHHHHHHHHHHHCCCCEEECCCCCCCHHHHHHHHHCCCCEEEECCCCCCHHHHHHHHHHCCCEEEEECCCCCCCHHHHHHHHHHHHHCCCEEECCCCHHHHHHHHHHCCCCHHCCCCCCCCCCCCCCCEECCCEEECCCCCCCEEE
> 1CSMB_1-256
MDFTKPETVLNLQNIRDELVRMEDSIIFKFIERSHFATCPSVYEANHPGLEIPNFKGSFLDWALSNLEIAHSRIRRFESPDETPFFPDKIQKSFLPSINYPQILAPYAPEVNYNDKIKKVYIEKIIPLISKRDGDDKNNFGSVATRDIECLQSLSRRIHFGKFVAEAKFQSDIPLYTKLIKSKDVEGIMKNITNSAVEEKILERLTKKAEVYGVDPTERRIIPEYLVKIYKEIVIPITKEVEVEYLLRRLEE
CCCCCHHHHCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHCCCCHHHCCCCCCCCHHHHHHHHHHHHHHHCCHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHCCCHHHHHHHHHHCCHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHCHHHHHHHHHCCCHHHHHHHHCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHCHHHHHHHHHHHHHHCHHHC
CCCCCCHHHCCHHHHHHHHHHHHHHHEHHHHHHCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEEHHHHHHHEHHHHCCEEECCCCCCCCCCCCHHHHHHHHHHHHHHHHEECCEEEHEHHHCCHCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHEECCCCHHHHHHHHHHHHCC
> 1HORB_1-266
MRLIPLTTAEQVGKWAARHIVNRINAFKPTADRPFVLGLPTGGTPMTTYKALVEMHKAGQVSFKHVVTFNMDEYVGLPKEHPESYYSFMHRNFFDHVDIPAENINLLNGNAPDIDAECRQYEEKIRSYGKIHLFMGGVGNDGHIAFNEPASSLASRTRIKTLTHDTRVANSRFFDNDVNQVPKYALTVGVGTLLDAEEVMILVLGSQKALALQAAVEGCVNHMWTISCLQLHPKAIMVCDEPSTMELKVKTLRYFNELEAENIKGL
CEEEECCCHHHHHHHHHHHHHHHHHHHCCCCCCCEEEEECCCCCCHHHHHHHHHHHHCCCCCCCCEEEEECEEECCCCCCCCCCHHHHHHHHCHHHCCCCHHHEECCCCCCCCHHHHHHHHHHHHHHHCCCCEEEECCCCCCCECCECCCCCCCCCCEEEECCHHHHHHHHHHHCCCHHHCCCEEEECCHHHHHCCCEEEEEECCHHHHHHHHHHHCCCCCCCCHHHHHHHCCCEEEEECCHHHCCCEHHHHHHHHHHHHHHHCCC
CEEEEECCHHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCHHHHHHHHHHHCCCCCCCCCEEEEEECCCEECCCCCCCCCHHHHHHHHHHHCCCCCHHCEECCCCCCCCHHHHHHHHHHHHHCCCCCEEEEECCCCCCCEECCCCCCCCCCCCEEEEHCCCCCECCCCCCCCCCCCCCCEEEEECHHHHHHHHEEEEEEECCCHHHHHHHHHCCCCCCCCCHHHHCCCCCEEEEEHHHHHHHCCCCCEHHHHHHHHHCCCCC
> 1HVQA_1-273
GGIAIYWGQNGNEGTLTQTCSTRKYSYVNIAFLNKFGNGQTPQINLAGHCNPAAGGCTIVSNGIRSCQIQGIKVMLSLGGGIGSYTLASQADAKNVADYLWNNFLGGKSSSRPLGDAVLDGIDFDIEHGSTLYWDDLARYLSAYSKQGKKVYLTAAPQCPFPDRYLGTALNTGLFDYVWVQFYNNPPCQYSSGNINNIINSWNRWTTSINAGKIFLGLPAAPEAAGSGYVPPDVLISRILPEIKKSPKYGGVMLWSKFYDDKNGYSSSILDSV
CEEEEEECCCHHHCCHHHHHHCCCEEEEEEEEEEEEECCEEEEECCCCCCCCHHHHHHHHHHHHHHHHHCCCEEEEEEECCCCCECCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEEECCCCCCCCHHHHHHHHHHHCCCCCCCEEEECCECCCCCCCCHHHHCCCCCCEEEEECCCCHHHCCECCECHHHHHHHHHHHHHCCCCEEEEEEECCHHHCCCCCCCHHHHHHHCHHHHCCCCCEEEEEEECHHHHHHHCHHHHHHHHC
CCEEEEECCCCCCCCCEHHCCCCCCEEEEEEEHECCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHCCCEEEEEECCCCCCCCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCHCCCCCEEEEEECCCCCHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCHHHHHHHHHCCHEEEEEEEECCCCCCCCCCCCHHHHHHHHHHHCCCCCCEEEEEECCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCCCEEEEHHHHCCCCCCCHEEHCCC
> 2CPOA_1-298
EPGSGIGYPYDNNTLPYVAPGPTDSRAPCPALNALANHGYIPHDGRAISRETLQNAFLNHMGIANSVIELALTNAFVVCEYVTGSDCGDSLVNLTLLAEPHAFEHDHSFSRKDYKQGVANSNDFIDNRNFDAETFQTSLDVVAGKTHFDYADMNEIRLQRESLSNELDFPGWFTESKPIQNVESGFIFALVSDFNLPDNDENPLVRIDWWKYWFTNESFPYHLGWHPPSPAREIEFVTSASSAVLAASVTSTPSSLPSGAIGPGAEAVPLSFASTMTPFLLATNAPYYAQDPTLGPND
CHHHCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCCEEECHHHHHHHHHHHHCECHHHHHHHHHHHHHHHHHHHCCCCCCCEEECHHHHCCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCHHHHHHHHHHHCCCCEECHHHHHHHHHHHHHHHHHHECCCCCCCCHHHHHHHHHHHHHHHECCCCCCCCCCCCEEHHHHHHHHHHCCCCHHHCCCCCCCCECHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHCCCHHHHHHHHHHHHECCCCCCCCCCCCCCCCHHHCCCCCCEEECCCCCCCHHHCCCCCCCCCHCCHHHHHHHHHHCCCCCCCCCCEHHHHHHHHHHHHHHCCCCCCCCCCCCHHCHHHHHHHHHEEEEECCCCCCCCCCCCCCCHHHHHHHHHHCCCCHHCCCCCCCCCCCCHHHHHHHHHHHHECCCCCCCCCCCCCCCCCCCEEEEEHHHHHCHHHHHCCCCCEECCCCCCCCC
> 3HMGA_1-328
QDLPGNDNSTATLCLGHHAVPNGTLVKTITDDQIEVTNATELVQSSSTGKICNNPHRILDGIDCTLIDALLGDPHCDVFQNETWDLFVERSKAFSNCYPYDVPDYASLRSLVASSGTLEFITEGFTWTGVTQNGGSNACKRGPGSGFFSRLNWLTKSGSTYPVLNVTMPNNDNFDKLYIWGIHHPSTNQEQTSLYVQASGRVTVSTRRSQQTIIPNIGSRPWVRGQSSRISIYWTIVKPGDVLVINSNGNLIAPRGYFKMRTGKSSIMRSDAPIDTCISECITPNGSIPNDKPFQNVNKITYGACPKYVKQNTLKLATGMRNVPEKQT
CCCCCCCCCCEEEEEEEECCCCCEEECCCCCCCEEECCEEECEECCCCCCEECCCCCEEECCCCCHHHHHHCCHHHHHHCCCECCEEEECCCCCCCCCCEECCCHHHHHHHHHHHCECCEEECCCCCCCEEEEECEEEEEECCEEECCCCEEEEEEECCECCCEEEEEECCCCCCEEEEEEEEECCCHHHHHHHHCCCCCCEEEECCCCEEEECCCCCCCCCECCECCEEEEEEEEECCCCEEEEEECCCEEEECEEEECCCCCCEEEECCCCEECCCCCEEECCEEECCCCCEECCCCCCEEECCEECCCCCCEEECCCECCCCCCC
CCCCCCCCCEEEEECCCCCCCCCCEEEEECCCCEEEHHHHHHHHHCCCCCECCCCCCEECCCCHHHHHHHCCCCCCCCCCCCCCEEEEECCHCCCCCCCCCCCCHHHHHEHEHHCCCHHHHHHCCCCEEEECCCCCCCEECCCCCCHHHEHCEEECCCCCCEEEEEECCCCCCCHHEEEECEECCCCCCHHHEHHEHCCCCEEEECCCCCCEECCCCCCCCCECCCCCCEEEEEEEECCCCEEEECCCCCEECCCCEEEECCCCCCEEECCCCCCCCCCEEECCCCCCCCCCCHHHHHCECCCCCCHHHHHCHHHHHCCCCCCCCCCC
> 1TRKB_3-331
QFTDIDKLAVSTIRILAVDTVSKANSGHPGAPLGMAPAAHVLWSQMRMNPTNPDWINRDRFVLSNGHAVALLYSMLHLTGYDLSIEDLKQFRQLGSRTPGHPEFELPGVEVTTGPLGQGISNAVGMAMAQANLAATYNKPGFTLSDNYTYVFLGDGCLQEGISSEASSLAGHLKLGNLIAIYDDNKITIDGATSISFDEDVAKRYEAYGWEVLYVENGNEDLAGIAKAIAQAKLSKDKPTLIKMTTTIGYGSLHAGSHSVHGAPLKADDVKQLKSKFGFNPDKSFVVPQEVYDHYQKTILKPGVEANNKWNKLFSEYQKKFPELGAELA
CCCHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCEEEECCHHHHHHHHHHHHHCCCCCCHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCECCECCCCCCEEEEECHHHHHCHHHHHHHHHHHHCCCCCEEEEEEECCEECCEEHHHCCCCCHHHHHHHCCCEEEEECCCCCCHHHHHHHHHHHHHCCCCCEEEEEECCCCCCCCCCCCHHHHCCCCCHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHCHHHHHHHH
CHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCCEEEEECCCCHHHHHHHHHHCCCCCCHHHHHHHCCCCCCCCCCCCCCCCCCEECCCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEEEEECCCCHCCCHHHHHHHHHHHHCCCCEEEEEECCCCECCCCCCCCCCCHHHHHHHHCCCEEEEECCCCCCHHHHHHHHHHHHHCCCCCEEEEEEEEECCCCHHHCCCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHHC
> 1BKSB_9-393
FGEFGGMYVPQILMPALNQLEEAFVRAQKDPEFQAQFADLLKNYAGRPTALTKCQNITAGTRTTLYLKREDLLHGGAHKTNQVLGQALLAKRMGKSEIIAETGAGQHGVASALASALLGLKCRIYMGAKDVERQSPNVFRMRLMGAEVIPVHSGSATLKDACNEALRDWSGSYETAHYMLGTAAGPHPYPTIVREFQRMIGEETKAQILDKEGRLPDAVIACVGGGSNAIGMFADFINDTSVGLIGVEPGGHGIETGEHGAPLKHGRVGIYFGMKAPMMQTADGQIEESYSISAGLDFPSVGPQHAYLNSIGRADYVSITDDEALEAFKTLCRHEGIIPALESSHALAHALKMMREQPEKEQLLVVNLSGRGDKDIFTVHDILKA
ECCEEEEECCHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCCEEECCCCCCCCCEEEEEEEHHHCCCCECCHHHHHHHHHHHHHCCCCEEEEEECCCHHHHHHHHHHHHHCCEEEEEEEHHHHHHCHHHHHHHHHCCCEEEEECCCCCCHHHHHHHHHHHHHHHCCCEEECCCCCCCCCCHHHHHHHCCCHHHHHHHHHHHHHCCCCCCEEEEECCCCHHHHHHHHHHCCCCCCEEEEEEEEECCHHHCCCCCHHHHCEEEEECEEEEEECECCCCCECCCCCCCHHHCCCECCHHHHHHHHCCCEEEEEEEHHHHHHHHHHHHHHHCCCECCCCHHHHHHHHHHHHHCCCCCEEEEEEECEECHHHHHHHHHHHCC
CCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCEEEEEHCCCCCCCCEECCHHHHHHHHHHHHCCCEEEEECCCCCCHHHHHHHHHHHCCEEEEEECCCCHHHCHHHHHHHHHCCCEEEEECCCCCCHHHHHHHHHHHHHHCCCCCEEEECCCCCCCCCCCCHCCCCCECCHHHHHHHHHHCCCCCCEEEEEECCCHHHHHHHHHHHCCCCCCCECCCCCCCCCCCCCCCCCCCCCCCCECCCCCCCHCCCCCCCCCCCHEEECCCCCCCCCHHHHHHHHCCCEEEEEECHHHHHHHHHHHHHHCCCECCCCHHHHHHHHHHHHHHCCCCCCEEEEEECCCCCCCHHHHHHHHCC
> 6CPPA_10-414
NLAPLPPHVPEHLVFDFDMYNPSNLSAGVQEAWAVLQESNVPDLVWTRCNGGHWIATRGQLIREAYEDYRHFSSECPFIPREAGEAYDFIPTSMDPPEQRQFRALANQVVGMPVVDKLENRIQELACSLIESLRPQGQCNFTEDYAEPFPIRIFMLLAGLPEEDIPHLKYLTDQMTRPDGSMTFAEAKEALYDYLIPIIEQRRQKPGTDAISIVANGQVNGRPITSDEAKRMCGLLLVGGLDTVVNFLSFSMEFLAKSPEHRQELIERPERIPAACEELLRRFSLVADGRILTSDYEFHGVQLKKGDQILLPQMLSGLDERENACPMHVDFSRQKVSHTTFGHGSHLCLGQHLARREIIVTLKEWLTRIPDFSIAPGAQIQHKSGIVSGVQALPLVWDPATTKAV
CCCCCCCCCCHHHECCCCCCCCCCHHHCHHHHHHHHHCCCCCCEEEECHHHCEEEECCHHHHHHHHHCCCCEECCCCCCCHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHCEEEHHHHCCCHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHCCEECCEECCHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCHHHHHHHHHCHHHHHHHHHHHHHHCCCECEEEEECCCEEECCEEECCCCEEEECHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHCCCCHHHHHHHHHHHHHHHHHHCCCCEECCCCCCCEECCCECEECCCEEECCHHHCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHCCCCEEEECCCCCCEEEEECHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHCHCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCEEEHHHHHCCCHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCCCCHHHHHHHHHHHHECCCCHHHHHHHHHHHHHHCCHHHHHHHHCCCCCHHHHHHHHHHHCCCCCCCCCCCCCEEECCCECCCCCEEEEEHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCECCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCCCCC
> 1OACB_301-727
PAVKPMQIIEPEGKNYTITGDMIHWRNWDFHLSMNSRVGPMISTVTYNDNGTKRKVMYEGSLGGMIVPYGDPDIGWYFKAYLDSGDYGMGTLTSPIARGKDAPSNAVLLNETIADYTGVPMEIPRAIAVFERYAGPEYKHQEMGQPNVSTERRELVVRWISTVGNXDYIFDWIFHENGTIGIDAGATGIEAVKGVKAKTMHDETAKDDTRYGTLIDHNIVGTTHQHIYNFRLDLDVDGENNSLVAMDPVVKPNTAGGPRTSTMQVNQYNIGNEQDAAQKFDPGTIRLLSNPNKENRMGNPVSYQIIPYAGGTHPVAKGAQFAPDEWIYHRLSFMDKQLWVTRYHPGERFPEGKYPNRSTHDTGLGQYSKDNESLDNTDAVVWMTTGTTHVARAEEWPIMPTEWVHTLLKPWNFFDETPTLGALKKDK
CCCCCCEEECCCCCCEEEECCEEEECCEEEEEEEECCCEEEEEEEEEEECCEEEEEEEEEEEEEEEEEECCCCCCCCCCEECHHHHHCCCCCECCCCCCCCCCCCCEEEEEEEECCCCCEEEEEEEEEEEEEEEEEEEEECCCCCCCEEEEEEEEEEEEEEEECCEEEEEEEEEECCCCEEEEEEEEECCCEEECCCCCCCCCCHHHHCCCEEEEECCEEEECEEEEEEEEEEECCCCCEEEEEEEEEEEEECCCCCCCCEEEEEEEEEECEHHHHCECCCCCCEEEEEEEEEECCCCCEEEEEEECCCEECCCECCCCCCCCCCHHHHHCHHHHCCEEEEECCCCCCCCCCCCCCCCCCCCCHHHHHHCCCECCCEEEEEEEEEEEEECCCHHHCCCEEEEEEEEEEEEECCCCCCCCCCEECCCC
CCCCCCEEECCCCCEEEECCCEEEEECCEEEEECCCCCCEEEEEEEECCCCCCEEHHHHHCHHHHEEECCCCCCCCHHCHHECCCCCCCCCCCCCCCCCCCCCCCEEEEEEEEECCCCCCEECCCEEEEEECCCCCCEEECCCCCCCHEECCCEEEEEEEEECCCHHHEEEEEECCCCCEEEEEEEECEEECCCCCCCCCCCCCCCCCCCCCCEECCCCECCHHHHHHEEEECCCCCCCCCEEEEEEEEECCCCCCCCCCCEEEEEEEEECCHHHHCCCCCCCCCEEEECCCCCCCCCCCCCEEEECCCCCCCCCCCCCCCCCCCCHHHHHHHHCCCEEEEECCCCCCCCCCCCCCCCCCCCCCHHHHCCCCCHCCCCEEEEEECCCCCCCCCCCCCCCCCHCCEEEECCCCCCCCCCCCCCCCCCC
> 2GLSA_1-468
SAEHVLTMLNEHEVKFVDLRFTDTKGKEQHVTIPAHQVNAEFFEEGKMFDGSSIGGWKGINESDMVLMPDASTAVIDPFFADSTLIIRCDILEPGTLQGYDRDPRSIAKRAEDYLRATGIADTVLFGPEPEFFLFDDIRFGASISGSHVAIDDIEGAWNSSTKYEGGNKGHRPGVKGGYFPVPPVDSAQDIRSEMCLVMEQMGLVVEAHHHEVATAGQNEVATRFNTMTKKADEIQIYKYVVHNVAHRFGKTATFMPKPMFGDNGSGMHCHMSLAKNGTNLFSGDKYAGLSEQALYYIGGVIKHAKAINALANPTTNSYKRLVPGYEAPVMLAYSARNRSASIRIPVVASPKARRIEVRFPDPAANPYLCFAALLMAGLDGIKNKIHPGEPMDKNLYDLPPEEAKEIPQVAGSLEEALNALDLDREFLKAGGVFTDEAIDAYIALRREEDDRVRMTPHPVEFELYYSV
CHHHHHHHHHHHCCCCEECCCCCCCCCCECCECCCCCCCCCHHHHCEEEECCCCCCCCCCEEEEEECCCCCEEECCCCCCCCEEECEECCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCCEEEEEEECEEEEECEEEEEEECCEEEEEEECCCCHHHCCCCCCCCCCCCCCCCCCCCCECHHHCCCCCHHHHHHHHHHHHCCCEEEEEECCCCCCEEEEEECCCCHHHHHHHHHHHHHHHHHHHHHHCCEEECCCCCCCCCCCCCCEEEEEEECCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHCCCCCCCCCEEEECCCCCCEEEECCCCCCHHHCCEEEECCCCCCCHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCECCCCHHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCCC
CHHHHHHHHHHCCCEEEEEEEECCCCCECEEEEEHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCEEECCCCCCEEECCCCCCCCEEEEEEEECCCCCCCCCCCCHHHHHHHHHHHHHCCCCCEEEECCCHHEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCCCHHHCCHHCCCCCCEEEEECCCCHHHHHHHHHHHHHHHHHHHHHCCCEEEECCCCCCCCCCCCCEEEEEEHCCCCCEECCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCCCCCHEEECCCCCCCEEEEECCCCCCEEEEECCCCCCCCCEEEECCCCCCCCHHHHHHHHHHHHHHCHHHCCCCCCCCCCCCCCCCCHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHECC
> 7CATA_3-500
NRDPASDQMKHWKEQRAAQKPDVLTTGGGNPVGDKLNSLTVGPRGPLLVQDVVFTDEMAHFDRERIPERVVHAKGAGAFGYFEVTHDITRYSKAKVFEHIGKRTPIAVRFSTVAGESGSADTVRDPRGFAVKFYTEDGNWDLVGNNTPIFFIRDALLFPSFIHSQKRNPQTHLKDPDMVWDFWSLRPESLHQVSFLFSDRGIPDGHRHMDGYGSHTFKLVNADGEAVYCKFHYKTDQGIKNLSVEDAARLAHEDPDYGLRDLFNAIATGNYPSWTLYIQVMTFSEAEIFPFNPFDLTKVWPHGDYPLIPVGKLVLNRNPVNYFAEVEQLAFDPSNMPPGIEPSPDKMLQGRLFAYPDTHRHRLGPNYLQIPVNCPYRARVANYQRDGPMCMMDNQGGAPNYYPNSFSAPEHQPSALEHRTHFSGDVQRFNSANDDNVTQVRTFYLKVLNEEQRKRLCENIAGHLKDAQLFIQKKAVKNFSDVHPEYGSRIQALLDKYN
CCCCHHHHHHHHHHCCCCCCCCCCECCCCCECCCCCCCCEECCCCCECCCCCCHHHHHHHHCCCCCCCCCCCCCEEEEEEEEEECCCCCCCCCCHHHCCCCCEEEEEEEEECCCCCCCCCCCCCCCCEEEEEEECCCCEEEEEEECCCCCCCCCHHHHHHHHHCCCECCCCCCECHHHHHHHHHHCHHHHHHHHHHCCCCCCECCHHHCCEECCCCEEEECCCCCEEEEEEEEEECCCCCECCHHHHHHHHHHCCCHHHHHHHHHHHCCCCCEEEEEEEEECHHHHHHCCCCCCCCCCCCCCCCCCCEEEEEEEEEECCCCHHHHCCCCCCCCCCCCCCEEECCCHHHHHHHHHHHHHHHHHCCCCHHHCHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHCCCCCCCCCCEECCCCCCCCCCHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCHHHHHHHHHHHHHHC
CCCCCHHHHHHHHHCCCCCCCCEEECCCCCCCCCCCCCCCCCCCCCEHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCEEEEEEECCCCCHHHHHHHHCCCCCCCEEEEEECCCCCCCCCCCCCCCCCHEEEEEECCCCCEEEEECCCCCEEECCHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHCCCHHHHHHHHHHCCCCCCCCHCCCCCCCCCEEEEEECCCCEEEEEEEECCCCCCCCCCHHHHHHCCCCCCCHHHHHHHHHHHCCCCCCEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCEEEEEEEEECCCCCCCHHHHHEECCCCCCCCCCCCCCCCCCHHCHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCECCCCEEEECCCCCCCCCCHHHHHHCCCCHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCHHHHHHHHHHHHHCC
