#!/bin/bash
# This script finds all the testSet and trainSet files in the current directory,
# converts them to matlab datasets and saves them in the folder mat_datasets.
folderName="mat_datasets"
mkdir "$folderName"

runAll=""
datasets=$(ls | grep -e 'testSet' -e 'trainSet' | grep -v '\.mat')

for ds in $datasets
do 
	# echo "$ds"
	loaded=$(echo "$ds" | sed "s:.txt::")
	# echo "$loaded"
	outFile=$(echo "./$folderName/$loaded.mat")
	# echo "$outFile"
	runMat="load $ds; y = $loaded(1:end, end); Z = $loaded(1:end, 1:end-1); save $outFile y Z -v7.3; clear;"
	# echo "$runMat"
	runAll="$runAll$runMat "
done 
runAll="$runAll exit;"
# echo "$runAll"
/Applications/MATLAB_R2019a.app/bin/matlab -nodisplay -r "$runAll" > "./$folderName/log.txt"